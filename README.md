# A Infinitestack Stackformation blueprint [Composer](http://getcomposer.org) component for Amazon VPCs


## howto Use

```
  - stackname: vpc
    description:  'VPC setup'
    template: '../components/vpc/vpc.template'
    parameters:
      #PeerVpcId: 'vpc-10e36f7b'
      #PeerVpcCidr: '172.31.0.0/16'
      Environment: '{env:ENVIRONMENT}'
      Name: 'Management'
      CidrPrefix: '{var:VpcCidrPrefix}'
```